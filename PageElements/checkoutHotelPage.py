import pytest
from _pytest import logging

from additional.waitMethods import *
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)


bill_to_client_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText'

first_option_in_bill_to_client_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]'

custom_field_1_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText'

first_option_in_custom_field_1_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]'

justification_field_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.EditText'

projects_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText'

first_option_in_projects_drop_down_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]'

finish_button = 'com.tripactions.alpha:id/finishButton'

view_trip_button = 'com.tripactions.alpha:id/view_trip_itinerary'



# bill to client drop down
def wait_bill_to_client_drop_down():
    logging.info(" Wait bill to client drop down")
    return wait_method_xpath(bill_to_client_drop_down_xpath)

def find_bill_to_client_drop_down_xpath():
    return driver.find_element_by_xpath(bill_to_client_drop_down_xpath)

@pytest.allure.step('Click find bill to client drop down')
def click_bill_to_client_drop_down():
    logging.info("Click find bill to client drop down")
    return find_bill_to_client_drop_down_xpath().click()


# first option in bill to client drop down
def wait_first_option_in_bill_to_client_drop_down():
    logging.info(" Wait first option in bill to client drop down")
    return wait_method_xpath(first_option_in_bill_to_client_drop_down_xpath)

def find_first_option_in_bill_to_client_drop_down():
    return driver.find_element_by_xpath(first_option_in_bill_to_client_drop_down_xpath)

@pytest.allure.step('Click first option in bill to client drop down')
def click_first_option_in_bill_to_client_drop_down():
    logging.info("Click first option in bill to client drop down")
    return find_first_option_in_bill_to_client_drop_down().click()

# custom field 1 drop down
def wait_custom_field_1_drop_down():
    logging.info(" Wait custom field 1 drop down")
    return wait_method_xpath(custom_field_1_drop_down_xpath)

def find_custom_field_1_drop_down():
    return driver.find_element_by_xpath(custom_field_1_drop_down_xpath)

@pytest.allure.step('Click custom field 1 drop down')
def click_custom_field_1_drop_down():
    logging.info("Click custom field 1 drop down")
    return find_custom_field_1_drop_down().click()

# click first option in custom field 1 drop down
def wait_first_option_in_custom_field_1_drop_down():
    logging.info(" Wait first option in custom 1 drop down")
    return wait_method_xpath(first_option_in_custom_field_1_drop_down_xpath)

def find_first_option_in_custom_field_1_drop_down():
    return driver.find_element_by_xpath(first_option_in_custom_field_1_drop_down_xpath)

@pytest.allure.step('Click first option in custom field 1 drop down')
def click_first_option_in_custom_field_1_drop_down():
    logging.info("Click first option in custom field 1 drop down")
    return find_first_option_in_custom_field_1_drop_down().click()

# enter value in justification field
def wait_justification_field():
    logging.info(" Wait justification field")
    return wait_method_xpath(justification_field_xpath)

def find_justification_field():
    return driver.find_element_by_xpath(justification_field_xpath)

@pytest.allure.step('Enter value in justification field')
def click_justification_field(justification):
    logging.info("Enter value in justification field")
    return find_justification_field().send_keys(justification)

# click projects drop down
def wait_projects_drop_down():
    logging.info(" Wait projects drop down")
    return wait_method_xpath(projects_drop_down_xpath)

def find_projects_drop_down():
    return driver.find_element_by_xpath(projects_drop_down_xpath)

@pytest.allure.step('Click projects drop down')
def click_projects_drop_down():
    logging.info("Click projects drop down")
    return find_projects_drop_down().click()


# first_option_in_projects_drop_down
def wait_first_option_in_projects_drop_down():
    logging.info(" Wait first option in projects drop down")
    return wait_method_xpath(first_option_in_projects_drop_down_xpath)

def find_first_option_in_projects_drop_down():
    return driver.find_element_by_xpath(first_option_in_projects_drop_down_xpath)

@pytest.allure.step('Select projects')
def click_first_option_in_projects_drop_down():
    logging.info("Select projects")
    return find_first_option_in_projects_drop_down().click()

# click finish button
def wait_finish_button():
    logging.info(" Wait finish button")
    return wait_to_click_by_id(finish_button)

def find_finish_button():
    return driver.find_element_by_id(finish_button)

@pytest.allure.step('Click finish button')
def click_finish_button():
    logging.info("Click finish button")
    return find_finish_button().click()

#  click view trip button
def wait_view_trip_button():
    logging.info(" Wait trip button")
    return wait_method(view_trip_button)

def find_view_trip_button():
    return driver.find_element_by_id(view_trip_button)

def click_view_trip_button():
    logging.info(" Click trip button")
    return find_finish_button().click()