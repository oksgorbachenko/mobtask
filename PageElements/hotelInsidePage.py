import pytest
from _pytest import logging

from additional.waitMethods import wait_method_xpath, wait_method
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)


view_rooms_button = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout'

expand_other_option_for_hotel_rooms = 'com.tripactions.alpha:id/caret'

first_room_in_suggested_rooms_list_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ExpandableListView/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]'

first_room_in_appartment_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ExpandableListView/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]'
checkout_hotel_button = 'com.tripactions.alpha:id/rightSideBar'

book_hotel_button = 'com.tripactions.alpha:id/rightSideBar'
x = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]'

# view rooms button in hotel page
def wait_view_rooms_button():
    logging.info(" Wait view rooms button ")
    return wait_method_xpath(view_rooms_button)

def find_view_rooms_button():
    return driver.find_element_by_xpath(view_rooms_button)

@pytest.allure.step('Click view rooms button')
def click_view_rooms_button():
    logging.info("Click view rooms button")
    return find_view_rooms_button().click()


# expand Other Option for hotel rooms
def wait_expand_other_option_for_hotel_rooms():
    return wait_method(expand_other_option_for_hotel_rooms)

def find_expand_other_option_for_hotel_rooms():
    return driver.find_element_by_id(expand_other_option_for_hotel_rooms)

@pytest.allure.step('Click expand other option for hotel rooms')
def click_expand_other_option_for_hotel_rooms():
    logging.info("Click expand other option for hotel rooms")
    return find_expand_other_option_for_hotel_rooms().click()

# first_room_in_suggested_rooms_list_xpath
def wait_first_room_in_suggested_rooms_list():
    logging.info(" Wait first variant in Other Options")
    return wait_method_xpath(first_room_in_suggested_rooms_list_xpath)

def find_first_room_in_suggested_rooms_list():
    return driver.find_element_by_xpath(first_room_in_suggested_rooms_list_xpath)

def click_first_room_in_suggested_rooms_list():
    logging.info(" Click first variant in Other Options")
    return find_first_room_in_suggested_rooms_list().click()

# click first room in list
def wait_first_room_in_list():
    logging.info(" Wait first room in list")
    return wait_method_xpath(first_room_in_appartment_xpath)

def find_first_room_in_list():
    return driver.find_element_by_xpath(first_room_in_appartment_xpath)

@pytest.allure.step('Click first room in list')
def click_first_room_in_list():
    logging.info("Click first room in list")
    return find_first_room_in_list().click()


# checkout hotel button
def wait_checkout_hotel_button():
    logging.info(" Wait checkout hotel button")
    return wait_method(checkout_hotel_button)

def find_checkout_hotel_button():
    return driver.find_element_by_id(checkout_hotel_button)

@pytest.allure.step('Click checkout hotel button')
def click_checkout_hotel_button():
    logging.info("Click checkout hotel button")
    return find_checkout_hotel_button().click()

# checkout hotel button
def wait_book_hotel_button():
    logging.info(" Wait book hotel button")
    return wait_method(book_hotel_button)

def find_book_hotel_button():
    return driver.find_element_by_id(book_hotel_button)

@pytest.allure.step('Click book hotel button')
def click_book_hotel_button():
    logging.info("Click book hotel button")
    return find_book_hotel_button().click()