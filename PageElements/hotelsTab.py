import pytest
from _pytest import logging

from additional.waitMethods import wait_method_xpath, wait_method
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)

location_field = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout'

find_hotel_button = 'com.tripactions.alpha:id/primary_search_button'

find_hotel_button_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.RelativeLayout/android.widget.Button'

city_address_search_field_id = 'com.tripactions.alpha:id/editText'

first_result_for_location_search = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView'

hotels_tab_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.a.c[2]'

check_in_field = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout'

ok_button_in_calendar ='android:id/button1'

# ok_button_in_calendar
def wait_ok_button_in_calendar():
    logging.info("Wait for ok button in calendar")
    return wait_method(ok_button_in_calendar)

def find_ok_button_in_calendar():
    return driver.find_element_by_id(ok_button_in_calendar)

@pytest.allure.step('Click to ok button in calendar')
def click_ok_button_in_calendar():
    logging.info('Click to ok button in calendar')
    return find_ok_button_in_calendar().click()


# first_result_for_location_search
def wait_check_in_field():
    logging.info("Wait for check in field")
    return wait_method_xpath(check_in_field)

def find_check_in_field():
    return driver.find_element_by_xpath(check_in_field)

@pytest.allure.step('Click to check in field')
def click_check_in_field():
    logging.info("Click to check in field")
    return find_check_in_field().click()


# first_result_for_location_search
def wait_first_result_for_location_search():
    logging.info("Wait first result for location search")
    return wait_method_xpath(first_result_for_location_search)

def find_first_result_for_location_search():
    return driver.find_element_by_xpath(first_result_for_location_search)

@pytest.allure.step('Click first result for location search')
def click_first_result_for_location_search():
    logging.info("Click first result for location search")
    return find_first_result_for_location_search().click()


# hotels tab
def wait_hotels_tab():
    return wait_method_xpath(hotels_tab_xpath)

def find_hotels_tab():
    return driver.find_element_by_xpath(hotels_tab_xpath)

@pytest.allure.step('Click to hotels tab')
def click_hotels_tab():
    logging.info(" Click to hotels tab")
    return find_hotels_tab().click()

# location field
def wait_hotel_location_field():
    logging.info(" Wait to location field ")
    return wait_method_xpath(location_field)

def find_hotel_location_field():
    return driver.find_element_by_xpath(location_field)

def open_hotel_location():
    logging.info(" Click to location field ")
    return find_hotel_location_field().click()

# city, address or lendmark search field
def wait_hotel_address_field():
    return wait_method(city_address_search_field_id)

def find_hotel_address_field():
    return driver.find_element_by_id(city_address_search_field_id)

def enter_hotel_address_location(location):
    logging.info(" Enter location ")
    return find_hotel_address_field().send_keys(location)

# find hotel button
def wait_find_hotels_button():
    logging.info(" Wait find hotels button")
    return wait_method(find_hotel_button)

def find_find_hotels_button():
    return driver.find_element_by_id(find_hotel_button)

@pytest.allure.step('Click find hotels button')
def click_find_hotels_button():
    logging.info(" Click find hotels button")
    return find_find_hotels_button().click()