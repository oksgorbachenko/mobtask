import pytest
from _pytest import logging

from additional.waitMethods import *
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)

new_trip_button = 'com.tripactions.alpha:id/newTripContainer'
new_trip_button_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout'


# New trip button
def wait_new_trip_button():
    logging.info(" Wait New trip button")
    return wait_to_click_by_xpath(new_trip_button_xpath)

def find_new_trip_button():
    return driver.find_element_by_xpath(new_trip_button_xpath)

@pytest.allure.step('Click New trip button')
def click_new_trip_button():
    logging.info(" Click New trip button")
    return find_new_trip_button().click()
