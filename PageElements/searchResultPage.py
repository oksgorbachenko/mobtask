import pytest
from _pytest import logging

from additional.waitMethods import wait_method_xpath, wait_method
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)


first_result_from_search = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout'

# continue button
def wait_first_result_from_search():
    logging.info(" Wait first result from search")
    return wait_method_xpath(first_result_from_search)

def find_first_result_from_search():
    return driver.find_element_by_xpath(first_result_from_search)

@pytest.allure.step('Click first result from search')
def click_first_result_from_search():
    logging.info(" Click first result from search")
    return find_first_result_from_search().click()