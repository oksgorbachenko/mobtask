import pytest
from _pytest import logging
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By

from additional.waitMethods import wait_method_xpath, wait_method
from config.configuration import driver, swipe

import logging
logging.basicConfig(level=logging.INFO)

image_class = 'android.widget.ImageView'

server_names = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout'

email_field = 'com.tripactions.alpha:id/email'

password_sign ='com.tripactions.alpha:id/loginWithEmailSection'

login_button ='com.tripactions.alpha:id/loginWithEmail'

password_field = 'com.tripactions.alpha:id/passwordEditText'

continue_button_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.Button'

continue_button_after_password_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.Button'


# continue button
def wait_continue_button():
    logging.info(" Wait continue button")
    return wait_method_xpath(continue_button_xpath)

def find_continue_button():
    return driver.find_element_by_xpath(continue_button_xpath)

@pytest.allure.step('Click continue button')
def click_continue_button():
    logging.info(" Click continue button")
    return find_continue_button().click()

# continue button after password
def wait_continue_after_psw_button():
    return wait_method_xpath(continue_button_after_password_xpath)

def find_continue_after_psw_button():
    return driver.find_element_by_xpath(continue_button_after_password_xpath)

@pytest.allure.step('Click continue button after user enter password')
def click_continue_after_psw_button():
    logging.info(" Click continue button after user enter password")
    return find_continue_after_psw_button().click()

# login button
def wait_login_button():
    logging.info(" Wait login button")
    return wait_method(login_button)

def find_login_button():
    return driver.find_element_by_id(login_button)

@pytest.allure.step('Click login button')
def click_login_button():
    logging.info(" Click login button")
    TouchAction(driver).tap(find_login_button()).perform()


# wain stage env
def wait_stage_env():
    logging.info(" Wait stage env buttons")
    return wait_method_xpath(server_names)

# password button
def wait_psw_button():
    logging.info(" Wait password button")
    return wait_method(password_sign)

def find_psw_button():
    return driver.find_element_by_id(password_sign)

@pytest.allure.step('Click pasword button')
def click_psw_btn():
    logging.info(" Click password button")
    return find_psw_button().click()


# email field
def find_email_field():
    return driver.find_element_by_id(email_field)

@pytest.allure.step('Enter email')
def enter_email(email):
    logging.info(" Enter email")
    return find_email_field().send_keys(email)

# password field
def wait_psw_field():
    logging.info(" Wait password field")
    return wait_method(password_field)

def find_password_field():
    return driver.find_element_by_id(password_field)

@pytest.allure.step('Enter password')
def enter_password(password):
    logging.info(" Enter password")
    return find_password_field().send_keys(password)

# Click to logo for change env = click by bounds because logo without ID
def find_image_class():
    return driver.find_elements_by_class_name(image_class)

@pytest.allure.step('Click to logo for change env = click by bounds because logo without ID')
def click_logo_img():
    logging.info(" Click logo")
    TouchAction(driver).tap(None, 619, 835, 1).perform()

# change stage to staging
def find_stage_server():
    return driver.find_element_by_xpath(server_names)

@pytest.allure.step('Click stage server')
def click_stage_server():
    logging.info(" Click stage server")
    return find_stage_server().click()

# for debugging
def get_contexts():
    print(driver.current_context)
    return driver.current_context

def get_page_source():
    print(driver.page_source)

# Method for take app in background
def take_app_to_back():
    driver.background_app(10)