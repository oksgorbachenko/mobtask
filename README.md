
This is a project with automated UI tests for TripAction app.

Needed software

In order to use this tests you need following packages:
```sh
- Python 3+
- Pip
- allure
- pytest
- appium server on 4777 port
- android studio with emulator os:'8.1'
- virtualenv
```

## Usage
Tests for the latest build of the application are possible only when generating a new .apk file in the directory ..config/App/

### How do I get set up env? ###
1. install virtualenv `pip install virtualenv`
2. install the environment into the project's root folder with Python3.6:
`virtualenv -p /path/to/systems/python3 env`
3. Source into the env: `source env/bin/activate`
4. Install all the requirements: `pip install -r requirements.txt`

Run tests
```
py.test Tests/login.py
```
OR with generating allure report
```
python3 -m pytest --alluredir reports Tests/login.py
```


------------------
Project structure
------------------

- In root directory of the project are placed run.py file, with script for run all tests and generation allure report. 

- Folder "additional" contains files with all needed methods for tests.
Tests refer to these files at the step of setup code


- Folder "additional" contains additional methods for the project

- Folder "config" contains files with configuration for tests and folder "App" contains files with .apk file with  project

- Folder "PageElements" contains files with page identificators.

- Folder "reports" contains auto-generated allure reports files

- Folders "Test" contain a set of files with tests. Each file contains test which checks some page functionality.

- requirmenst.txt contain all lib for project


## License

Reinvently © [Oksana Gorbachenko](ogorbachenko@reinvently.com)
