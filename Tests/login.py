from time import sleep

import allure
import pytest
from pytest_testrail.plugin import testrail, pytestrail

from PageElements.checkoutHotelPage import *
from PageElements.hotelInsidePage import *
from PageElements.hotelsTab import *
from PageElements.myTripsPage import *
from PageElements.searchResultPage import *
from PageElements.startPageElements import *
from config.configuration import driver, tearDown
from  PageElements import startPageElements


# for connection with testrail
# @pytestrail.case('')
@pytest.allure.step('Login in app')
def test_login_in_app():
    # User sleep because logo img without locator - it is impossible to use waiter
    sleep(5)
    # Select server
    click_logo_img()
    wait_stage_env()
    click_stage_server()
    # There some issue in app or appium- after test select server,
    # popup with env disappears but page source stay as for page with popup
    take_app_to_back()
    # Wait and click to 'Continue with Email'
    wait_login_button()
    click_login_button()
    # Enter email
    enter_email("testtripactions+1Aug291352JV@gmail.com")
    wait_continue_button()
    click_continue_button()
    wait_psw_button()
    click_psw_btn()
    wait_psw_field()
    enter_password("PetrenkoLcIg1")
    wait_continue_after_psw_button()
    click_continue_after_psw_button()

    # My trip screen
    # it is not clear when the button becomes active, sometimes after clicking there is no response
    wait_new_trip_button()
    click_new_trip_button()

    # Tap to Hotels Tab
    wait_hotels_tab()
    click_hotels_tab()

    # Enter hotel location and date
    wait_hotel_location_field()
    open_hotel_location()
    wait_hotel_address_field()
    enter_hotel_address_location(location="Los Angeles")
    wait_first_result_for_location_search()
    click_first_result_for_location_search()

    wait_check_in_field()
    click_check_in_field()
    # check in
    wait_ok_button_in_calendar()
    click_ok_button_in_calendar()
    # check out
    # double OK in case when check out calendar popup appears after user choose check in date
    # need to clarify when it appears and when not, for now added check is_enabled find hotels button

    if find_find_hotels_button().is_enabled():
        action_after_select_location()

    else:
        wait_ok_button_in_calendar()
        click_ok_button_in_calendar()
        action_after_select_location()


def action_after_select_location():
    # Click Find hotel button
    wait_find_hotels_button()
    click_find_hotels_button()

    # click to first hotel from suggested list
    wait_first_result_from_search()
    click_first_result_from_search()

    # Click View rooms
    wait_view_rooms_button()
    click_view_rooms_button()

    # Expand other options for apartment
    wait_expand_other_option_for_hotel_rooms()
    click_expand_other_option_for_hotel_rooms()

    # Click to first room button
    wait_first_room_in_list()
    click_first_room_in_list()

    # Click checkout button
    wait_checkout_hotel_button()
    click_checkout_hotel_button()

    # Book hotel button
    wait_book_hotel_button()
    click_book_hotel_button()

    # Checkout
    # bill to client
    wait_bill_to_client_drop_down()
    click_bill_to_client_drop_down()
    wait_first_option_in_bill_to_client_drop_down()
    click_first_option_in_bill_to_client_drop_down()
    # Custom field 1
    wait_custom_field_1_drop_down()
    click_custom_field_1_drop_down()
    wait_first_option_in_custom_field_1_drop_down()
    click_first_option_in_custom_field_1_drop_down()
    # Justification
    wait_justification_field()
    click_justification_field("Some justification is here" + "\n")
    driver.hide_keyboard()
    # Project
    wait_projects_drop_down()
    click_projects_drop_down()
    wait_first_option_in_projects_drop_down()
    click_first_option_in_projects_drop_down()
    # Finish
    wait_finish_button()
    click_finish_button()
    tearDown()
