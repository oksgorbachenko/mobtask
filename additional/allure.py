import json

import allure
from allure_commons.types import AttachmentType

from config.configuration import driver


def allure_attach():
    allure.attach(driver.get_screenshot_as_png())
