from selenium.webdriver.common.by import By

from config.configuration import driver, swipe
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import logging
logging.basicConfig(level=logging.INFO)


# waiters- by id and xpath
def wait_method(locator):
    WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.ID, locator)))

def wait_method_xpath(locator):
    WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, locator)))

def wait_to_click_by_id(locator):
    WebDriverWait(driver, 40).until(EC.element_to_be_clickable((By.ID, locator)))

def wait_to_click_by_xpath(locator):
    WebDriverWait(driver, 40).until(EC.element_to_be_clickable((By.XPATH, locator)))

