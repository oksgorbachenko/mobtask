import os

from _pytest import logging
from appium import webdriver

cap = {}
cap['platformName'] = 'Android'
cap['platformVersion'] = '8.1'
cap['deviceName'] = 'emulator-5554'
cap['app'] = os.path.join(os.path.dirname(os.path.abspath(__file__)), "App/TripActions.apk")
cap['clearSystemFiles'] = True
cap['appActivity'] = 'com.tripactions.alpha.Activities.FirstSteps.Checkpoint'
# cap['appActivity'] ='com.tripactions.alpha.Activities.FirstSteps.WarmWelcome'

# cap['fullReset'] = True
# cap['noReset'] = True
server_url = 'http://127.0.0.1:4723/wd/hub'
driver = webdriver.Remote(server_url, cap)


def tearDown():
    return driver.quit()

def swipe():
    return driver.swipe(100, 500, 100, -500, 1)

def swipe_up():
    return driver.swipe(100, 100, 100, 500, 1)

def swipe_up_tabs():
    return driver.swipe(100, 400, 100, 1700, 1)